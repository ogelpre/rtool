#!/usr/bin/env python3

import requests
import copy
import json
import datetime
import random
from .helpers import debug

API_VERSION="v1"
API_FORMAT="json"

class Session(object):
    def __init__(self, url, auth_token=None, account_id=None, format=API_FORMAT):
        self.url = url
        self.version = API_VERSION
        self.auth_token = auth_token
        self.account_id = account_id
        self.format = format
        self.__session = requests.Session()

    def raw_request(self, service, method, data):
        if self.format is not API_FORMAT:
            raise NotImplementedError('{format} format not supported'.format(format=self.format))

        url = self.url.format(service=service, version=self.version,
                              format=self.format, method=method)

        req = requests.Request('POST', url,
            headers={ "Content-Type": "application/json" }, data=data)
        prepared = req.prepare()

        debug(2, ('URL', url), ('HEADER', prepared.headers), ('BODY', prepared.body))

        return self.__session.send(prepared)

    def request(self, service, method, data=None, client_transaction_id=None,
        auth_token=None, account_id=None):

        if auth_token is None:
            auth_token = self.auth_token
        if account_id is None:
            account_id = self.account_id

        if data is None:
            data = {}
        else:
            data = copy.copy(data)
        if auth_token is not None:
            data["authToken"] = auth_token
        if account_id is not None:
            data["ownerAccountId"] = account_id
        if client_transaction_id is None:
            d = datetime.datetime.utcnow().strftime("%Y%m%d%H%M%S")
            u = ''.join(random.SystemRandom().choice('0123456789abcdef') for _ in range(8))
            data["clientTransactionId"] = "{}-{}".format(d, u)
        else:
            data["clientTransactionId"] = client_transaction_id

        url = self.url.format(service=service, version=self.version,
                              format=self.format, method=method)

        ldata = json.dumps(data)

        return self.raw_request(service, method, ldata)

    def close(self):
        self.session.close()
