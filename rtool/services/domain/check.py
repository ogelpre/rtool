from ..raw import base

class Method(base.Method):
    def service(self):
        return 'domain'

    def method(self):
        return 'domainStatus'

    def data(self):
        data = {}
        data['domainNames'] = self.args.domain
        return data

    def format_default_success(self, dct):
        data = []
        for r in dct['responses']:
            data.append('{domainNameUnicode} is {status}'.format(**r))
        return '\n'.join(data)
