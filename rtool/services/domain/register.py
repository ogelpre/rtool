from ..raw import base
from ... import helpers
from . import price
import copy
import sys

class Method(base.Method):
    def service(self):
        return 'domain'

    def method(self):
        if self.args.transfer:
            return 'domainTransfer'
        else:
            return 'domainCreate'

    def confirmation(self):
        self.args.tld = [self.args.domain.split('.',1).pop()]
        price.Method(self.config, self.args, self.session).execute()
        del self.args.tld
        if not helpers.prompt("Register '{domain}'?".format(domain=self.args.domain), default='no', force=[None,'yes'][self.args.yes]):
            print('Aborting')
            sys.exit(1)

    def execute(self):
        if not self.args.quiet:
            self.confirmation()
        elif not self.args.yes:
            sys.exit(1)
        result = self.request()
        self.print_result(result)

    def data(self):
        data = {}
        data['domain'] = {}
        data['domain']['name'] = self.args.domain
        data['domain']['transferLockEnabled'] = True
        data['domain']['contacts'] = self.contacts()
        data['domain']['nameservers'] = self.nameservers()
        data["dnsSec"] = self.dnskey()
        if self.args.transfer:
            data["transferData"] = { "authInfo": self.args.transfer }
        return data

    def contacts(self):
        c = list()
        args = vars(self.args)
        if self.args.domain.split(".",1).pop() in ["de", "it"]:
            cl = ['owner', 'admin', 'tech', 'zone']
        else:
            cl = ['owner', 'admin', 'tech']
        for index, contact in enumerate(cl):
            if contact in args and args[contact]:
                p = {'contact':args[contact], 'type':contact}
            elif 'domain' in self.config and contact in self.config['domain']:
                p = {'contact':self.config['domain'][contact], 'type':contact}
            elif index == 0:
                raise ValueError('owner contact is mandatory')
            else:
                p['type'] = contact
            c.append(copy.deepcopy(p))
        return c

    def nameservers(self):
        ns_raw = []
        if 'nameserver' in self.args and self.args.nameserver:
            ns_raw = self.args.nameserver
        elif 'domain' in self.config and 'nameserver' in self.config['domain']:
            ns_raw = self.config['domain']['nameserver']
        ns_data = []
        for ns in ns_raw:
            ns_object = {}
            ns_split = ns.split()
            ns_object['name'] = ns_split.pop(0)
            ns_object['ips'] = ns_split
            ns_data.append(ns_object)
        return ns_data

    def dnskey(self):
        k = []
        for key in self.args.key:
           dk = {}
           dk['flags'] = int(key[0])
           dk['protocol'] = int(key[1])
           dk['algorithm'] = int(key[2])
           dk['publicKey'] = key[3]
           k.append({'keyData':dk})
        return k

    def format_default_success(self, dct):
        message = []
        message.append("domain: {}".format(dct['response']['name']))
        message.append("status: {}".format(dct['status']))
        if dct['warnings']:
            message.append('There are warnings. Pleas check message below.')
            message.append(self.format_yaml(dct['warnings']))
        return "\n".join(message)
