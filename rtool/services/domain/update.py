from . import show
import sys

class Method(show.Method):
    def method(self):
        return 'domainUpdate'

    def data(self):
        try:
            d = show.Method(self.config, self.args, self.session).request()['response']
        except KeyError:
            print("domain {domain} not found".format(domain=self.args.domain))
            sys.exit(1)
        data = {'domain': d}
        data['actingAs'] = 'designatedAgent'
        self.contacts(data)
        self.nameserver(data)
        self.dnskey(data)
        return data

    def contacts(self, data):
        args = vars(self.args)
        for c in data['domain']['contacts']:
            if c['type'] in args and args[c['type']] is not None:
                c['contact'] = args[c['type']]

    def nameserver(self, data):
        args = vars(self.args)
        if 'nameserver' in args and args['nameserver']:
            data['domain']['nameservers'] = []
            for ns in args['nameserver']:
                ns_split = ns.split()
                ns_object = {}
                ns_object['name'] = ns_split.pop(0)
                ns_object['ips'] = ns_split
                data['domain']['nameservers'].append(ns_object)

    def dnskey(self, data):
        args = vars(self.args)
        if 'newkey' in args and args['newkey']:
            data['dnsSecKeyAdd'] = []
            for key in args['newkey']:
                if any(key):
                    dk = {}
                    dk['flags'] = int(key[0])
                    dk['protocol'] = int(key[1])
                    dk['algorithm'] = int(key[2])
                    dk['publicKey'] = key[3]
                    data['dnsSecKeyAdd'].append({'keyData':dk})
        if 'removekey' in args and args['removekey']:
            data['dnsSecKeyRemove'] = []
            for key in data['domain']['dnsSecEntries']:
                if str(key['keyTag']) in args['removekey']:
                    data['dnsSecKeyRemove'].append(key)
