from ..raw import base
ORDER=['renew', 'create', 'transfer', 'update', 'ownerChange', 'restore']

class Method(base.Method):
    def service(self):
        return 'billing'

    def method(self):
        return 'priceListDomains'

    def data(self):
        return {}

    def format_default_success(self, dct):
        m = []
        for i,tld in enumerate(dct['responses']):
            t = self.tld(tld)
            if i == 0:
                m.extend(self.header())
            if 'tld' in self.args and self.args.tld and t['suffix'] not in self.args.tld:
                continue
            if not ('tld' in self.args and self.args.tld) and not i == 0 and not i%20:
                m.extend(self.header())
            c = []
            for x in ORDER:
                if t[x] is None:
                    y = "{:^21}".format("N/A")
                else:
                    y = "{netto:7.2f}{currency:3} {brutto:7.2f}EUR".format(netto=t[x][0],brutto=t[x][1],currency=t['currency'])
                c.append(y)
            s = "{suffix: <18}|{x}".format(suffix=t['suffix'],x=" |".join(c))
            m.append(s)
        return "\n".join(m)

    def tld(self, tld):
        t = {}
        t['suffix'] = tld['domainSuffix']
        if tld['currency'] is None:
            t['currency'] = 'N/A'
        else:
            t['currency'] = tld['currency'].upper()
        for x in ORDER:
            if tld[x] is not None:
                netto = tld[x]/10000.0
                if tld['exchangeRatio'] is None:
                    brutto = tld[x]/10000.0*(1+tld['vatRate']/10000)
                else:
                    brutto = tld[x]/10000.0*(1+tld['vatRate']/10000)*tld['exchangeRatio']['exchangeRatio']/10000.0
                t[x] = [netto, brutto]
            else:
                t[x] = None
        return t

    def header(self):
        m = []
        c = []
        for x in ORDER:
             y = "{:^21}".format(x)
             c.append(y)
        s = "{suffix: <18}|{x}".format(suffix='suffix',x=" |".join(c))
        m.append("="*len(s))
        m.append(s)
        m.append("="*len(s))
        return m
