from . import show
import sys

class Method(show.Method):
    def method(self):
        return 'zoneUpdate'

    def data(self):
        data = {}
        try:
            data['zoneConfig'] = show.Method(self.config, self.args, self.session).request()['response']['data'][0]
        except IndexError:
            print("zone {zone} not found".format(zone=self.args.domain))
            sys.exit(1)
        if 'master' in self.args and self.args.master is not None:
            data['zoneConfig']['masterIp'] = self.args.master
        return data

    def format_default_success(self, dct):
        z = [{'type': dct['response']['zoneConfig']['type'].lower()}, {'master':  dct['response']['zoneConfig']['masterIp']}]
        zone = {dct['response']['zoneConfig']['nameUnicode']:z}
        return self.format_yaml(zone)
