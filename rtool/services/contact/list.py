from ..raw import base

ATTR=['type','name','organization','street','postalCode','city',
    'country','emailAddress','phoneNumber','faxNumber']

class Method(base.Method):
    def service(self):
        return 'domain'

    def method(self):
        return 'contactsFind'

    def data(self):
        data = {}
        data['limit'] = 0
        data['sort'] = {"field": "ContactName", "order": "asc"}
        filter = []
        if 'hidden' in self.args and not self.args.hidden:
            filter.append(('ContactHidden', 'false'))
        data.update(self.filter(*filter))
        return data

    def format_default_success(self, dct):
        contacts = []
        for contactobj in dct['response']['data']:
            contacts.append(self.format_yaml(self.f_contact(contactobj)))
        return "\n".join(contacts)

    def f_contact(self, dct, attr=ATTR):
        data=[]
        for d in attr:
            data.append({d:dct[d]})
        contact = {dct['handle']: data}
        return contact
