# rtool

rtool ist ein Kommandozeilenwerkzeug für die JSON API von http.net und hosting.de.

Benutzung dieses Programms erfolgt auf eigene Gefahr!
Für Schäden an Personen, Haustieren oder materiellen Dingen (z.B. HiWis)  wird keine Haftung übernommen.

## Benutzung

rtool ist in mehrere Bereiche gegliedert. In jedem Bereich gibt es wiederum Befehle.

```
$ rtool [Basisoptionen] {Bereich} [Bereichsoptionen] {Befehl} [Befehlsoptionen]
```

An den jeweiligen stellen der Optionen kann man `-h` oder `--help` verwenden,
um eine Liste der Optionen zu erhalten.`

Welche Bassisoptionen und Bereiche es gibt, kann mit `rtool -h` herausgefunden werden.
Sollen die Optionen für den Bereich *domain* ausgegeben werden, so kann `rtool domain -h` verwendet werden.
Für die Befehlsoptionen muss wiederum einfach `-h` angehängt werden.

### Basisoptionen

`rtool` liest seine globale Konfiguration aus */etc/rtool/rtool.yml* und ergänzt diese um die
benutzerspezifische Konfiguration *~/.config/rtool/rtool.yml*.
Ein Konfigurationsbeispiel ist in der Datei *example.yml zu finden.
Die benutzerspezifische Konfigurationsdatei kann mit der Option `--config CONFIG` geändert werden.
Alle Optionen können durch Parameter überschrieben werden.

Damit ein Befehl ausgeführt werden kann, muss mindestens die API-URL und der Token angegeben werden.

Die Ausgabe ist in der Regel vereinfacht. Mit der Option `--output` kann die erweitere Ausgabe
aktiviert werden. Als Format stehen yaml, json und pprint bereit. pprint gibt die interne dict-Struktur wieder.

#### rtool domain list [--contact CONTACT]
Mit diesem Befehl werden Domains aufgelistet. Mit dem Filter --contact werden nur Domains, die den angegebenen Kontakt verwenden, aufgelistet.

#### rtool domain show domain.tld
Mit diesem Befehl werden Details zur Domain domain.tld aufgezeigt.

#### rtool domain check domain.tld [domain.tld ...]
Mit diesem Befehl wird geprüft, ob eine Domain noch frei ist.

#### rtool domain price [tld [tld ...]]
Mit diesem Befehl wird die komplette Preisliste ausgegeben.
Optional kann nach bestimmten TLDs gefiltert werden.
Die erste Spalte gibt jeweils den Netto Preis an. Die zweite Spalte gibt den Brutto Preis in Euro an.

#### rtool domain register [--owner contact] [--admin contact] [--tech contact] [--zone contact] [--transfer auth_info] [--nameserver NAMESERVER] [--key flags protocol algorithm key] [--yes] [--quiet] domain.tld
Mit diesem Befehl werden Domains registriert beziehungsweise mit der Option `--transfer` transferiert. 
Es werden die Kosten angezeigt und noch einmla eine Bestätigung eingeholt. Mit `--yes` wird autoamtisch bestätigt. Mit `--quiet` kann die zusätzliche Ausgabe des Preises unterbunden werden.

#### rtool domain update  [--owner contact] [--admin contact] [--tech contact] [--zone contact] [--nameserver NAMESERVER] [--newkey flags protocol algorithm key] [--removekey tag] domain.tld
Mit diesem Befehl werden die Domainangaben aktualisiert.
 
#### rtool domain jobs [--sid ID] [--cid ID] [--failed] [--running]
Mit diesem Befehl werden Aufträge angezeigt. Es ist möglich nach ClientID, ServerID, laufenden und fehlgeschlagenen Aufgaben zu filtern.

#### rtool contact list [--hidden]
Mit diesem Befehl werden angelegte Kontakte aufgelistet. Mit `--hidden` werden auch versteckte Kontakte angezeigt.

#### rtool contact show contact
Mit diesem Befehl werden Details zu einem Kontakt ausgegeben.

#### rtool contact create [--type {person,org,role}] [--name NAME] [--organization ORGANIZATION] [--street STREET] [--postalCode POSTALCODE] [--city CITY] [--country COUNTRY] [--emailAddress EMAILADDRESS] [--phoneNumber PHONENUMBER] [--faxNumber FAXNUMBER]
Mit diesem Befehl wird ein neuer Kontakt angelegt.

#### rtool contact update [--street STREET] [--postalCode POSTALCODE] [--city CITY] [--country COUNTRY] [--emailAddress EMAILADDRESS] [--phoneNumber PHONENUMBER] [--faxNumber FAXNUMBER] contact
Mit diesem Befehl wird ein Kontakt aktualisiert.

#### rtool contact delete contact
Mit diesem Befehl wird ein Kontakt versteckt. Löschen ist technisch nicht möglich. Nicht benutzte Kontakte werden hin und wieder automatisch vom System entfernt.

#### rtool zone list
Mit diesem Befehl werden die konfigurierten Zonen ausgegeben.

#### rtool zone show domain.tld
Mit diesem Befehl werden die Details zu einer konfigurierten Zone ausgegeben.

#### rtool zone create [--master MASTER] domain.tld
Mit diesm Befehl wird eine Zone angelegt.

#### rtool zone update [--master MASTER] domain.tld
Mit diesem Befehl wird eine Zone modifziziert.

#### rtool zone delete domaint.tld
Mit diesem Befehl wird eine Zone gelöscht.

#### rtool zone jos [--sid ID] [--cid ID] [--failed] [--running]
Mit diesem Befehl können Zone-Aufträge angezeigt werden.

### rtool raw

Hier ein Beispiel wie man die RAW-Schnittstelle verwenden kann.
<pre>
echo '{"contact": {"name": "Hostmaster Of The Day", "street": ["Stree 1"], "faxNumber": "+49 123 456789", "phoneNumber": "+49 234 56789", "postalCode": "9999", "country": "de", "emailAddress": "hostmaste@example.local", "organization": "ACME Corp.", "city": "Dampfhausen", "type": "role"}}' | /usr/local/netadmin/rtool raw --lazy domain contactCreate
</pre>

Die `--lazy`-Option setzt überschreibt die Client-ID und das Token.
